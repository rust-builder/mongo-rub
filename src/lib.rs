//! Rust Builder for [Mongo DB](https://github.com/mongo-db/mongo).
//!
//! This crate is intended for use with
//! [rub](https://github.com/rust-builder/rub).
//!
//! If you don't have `rub` installed, visit https://github.com/rust-builder/rub
//! for installation instructions.
//!
//! # Rub Options
//! <pre>
//! $ rub mongo --help
//! mongo - Rust Builder
//!
//! Usage:
//!     rub mongo [options] [&lt;lifecycle&gt;...]
//!     rub mongo (-h | --help)
//!     rub mongo --version
//!
//! Options:
//!     -d --dir &lt;dir&gt;        Set the projects directory.
//!     -b --branch &lt;branch&gt;  Set the build branch. [default: master]
//!     -t --enable-test      Enable tests.
//!     -p --prefix &lt;prefix&gt;  Set the installation prefix. [default: /usr/local]
//!     -u --url &lt;url&gt;        Set the SCM URL.
//!     -h --help             Show this usage.
//!     --version             Show rust-rub version.
//! </pre>
//!
//! # Examples
//! ```
//! # extern crate buildable; extern crate mongo_rub; fn main() {
//! use buildable::Buildable;
//! use mongo_rub::MongoRub;
//!
//! // To run lifecycle methods outside of rub...
//! let mut mr = MongoRub::new();
//! let b = Buildable::new(&mut mr, &vec!["rub".to_string(),
//!                                       "mongo".to_string(),
//!                                       "--version".to_string()]);
//! assert_eq!(Ok(0), b.version());
//! # }
//! ```
#![experimental]
#![allow(unstable)]
extern crate buildable;
extern crate commandext;
extern crate docopt;
extern crate regex;
extern crate "rustc-serialize" as rustc_serialize;
extern crate scm;
extern crate utils;

use buildable::{Buildable,BuildConfig,LifeCycle};
use commandext::{CommandExt,to_res};
use docopt::Docopt;
use regex::Regex;
use scm::git::GitCommand;
use std::default::Default;
use std::io::fs;
use std::io::fs::PathExtensions;
use utils::usable_cores;
use utils::empty::to_opt;

static USAGE: &'static str = "mongo - Rust Builder

Usage:
    rub mongo [options] [<lifecycle>...]
    rub mongo (-h | --help)
    rub mongo --version

Options:
    -d --dir <dir>        Set the projects directory.
    -b --branch <branch>  Set the build branch. [default: master]
    -t --enable-test      Enable tests.
    -p --prefix <prefix>  Set the installation prefix. [default: /usr/local]
    -u --url <url>        Set the SCM URL.
    -h --help             Show this usage.
    -l --lint             Run the MongoDB linter.
    --mst=<mst>           MongoDB scons targets.
                          core, tools, and all supported.
    --smoke               Runs the “dbtest” test.
    --smokec              Runs the C++ unit tests.
    --smokej              Runs (some of!) the Javascript integration tests.
    --version             Show rust-rub version.";

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[derive(RustcDecodable)]
struct Args {
    flag_dir: String,
    flag_branch: String,
    flag_enable_test: bool,
    flag_prefix: String,
    flag_url: String,
    flag_help: bool,
    flag_lint: bool,
    flag_mst: String,
    flag_smoke: bool,
    flag_smokec: bool,
    flag_smokej: bool,
    flag_version: bool,
    arg_lifecycle: Vec<String>,
}

/// Mongo specific configuration for the `Buildable` lifecycle methods.
#[experimental]
#[derive(Clone,Default)]
pub struct MongoRub {
    config: BuildConfig,
    prefix: String,
    url: String,
    lint: bool,
    targets: Vec<String>,
    smoke: bool,
    smokec: bool,
    smokej: bool,
}

impl MongoRub {
    /// Create a new default MongoRub.
    pub fn new() -> MongoRub {
        Default::default()
    }
}

fn is_v26(branch: &str) -> bool {
    let re = Regex::new(r"^[rv]2\.6").unwrap();
    re.is_match(branch)
}

impl Buildable for MongoRub {
    /// Update the MongoRub struct after parsing the given args vector.
    ///
    /// Normally, the args vector would be supplied from the command line, but
    /// they can be supplied as in the example below as well.
    ///
    /// # Example
    /// ```
    /// # extern crate buildable; extern crate mongo_rub; fn main() {
    /// use buildable::Buildable;
    /// use mongo_rub::MongoRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut mr = MongoRub::new();
    /// let b = Buildable::new(&mut mr, &vec!["rub".to_string(),
    ///                                       "mongo".to_string()]);
    /// assert_eq!(Ok(0), b.version());
    /// # }
    fn new(&mut self, args: &Vec<String>) -> &mut MongoRub {
        let dargs: Args = Docopt::new(USAGE)
            .and_then(|d| Ok(d.help(false)))
            .and_then(|d| d.argv(args.clone().into_iter()).decode())
            .unwrap_or_else( |e| e.exit());
        self.prefix = dargs.flag_prefix;
        self.url = dargs.flag_url;
        self.lint = dargs.flag_lint;
        self.smoke = dargs.flag_smoke;
        self.smokec = dargs.flag_smokec;
        self.smokej = dargs.flag_smokej;

        if !dargs.flag_mst.is_empty() {
            let v: Vec<&str> = dargs.flag_mst.split(',').collect();

            self.targets = v.iter().map(|s| s.to_string()).collect();
        } else {
            self.targets = Vec::new();
        }

        if dargs.flag_version {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["version"]);
            self.config = cfg;
        } else if dargs.flag_help {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["help"]);
            self.config = cfg;
        } else {
            let mut cfg = BuildConfig::new();

            if to_opt(dargs.flag_dir.as_slice()).is_some() {
                cfg.dir(Path::new(dargs.flag_dir.as_slice()));
            }
            cfg.project("mongo");
            if to_opt(dargs.flag_branch.as_slice()).is_some() {
                cfg.branch(dargs.flag_branch.as_slice());
            }
            cfg.test(dargs.flag_enable_test);

            let lc = dargs.arg_lifecycle;
            if to_opt(lc.clone()).is_some() {
                let mut mylc = Vec::new();
                for lc in lc.iter() {
                    mylc.push(lc.as_slice());
                }
                cfg.lifecycle(mylc);
            }
            self.config = cfg;
        }
        self
    }

    /// Get the `BuildConfig` associated with the `MongoRub`.
    ///
    /// # Example
    /// ```
    /// # extern crate buildable; extern crate mongo_rub; fn main() {
    /// use buildable::Buildable;
    /// use mongo_rub::MongoRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut mr = MongoRub::new();
    /// let b = Buildable::new(&mut mr, &vec!["rub".to_string(),
    ///                                       "mongo".to_string()]);
    /// let bc = b.get_bc();
    /// assert_eq!("mongo", bc.get_project());
    /// # }
    fn get_bc(&self) -> &BuildConfig {
        &self.config
    }

    /// No lifecycle reorder is necessary for Rust.
    fn reorder<'a>(&self, lc: &'a mut Vec<LifeCycle>) -> &'a mut Vec<LifeCycle> {
        lc
    }

    /// Check for cargo dependencies.
    ///
    /// TODO: Implement
    fn chkdeps(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Peform the git operations necessary to get the project directory ready
    /// for the rest of the build lifecycle operations.
    ///
    /// # Notes
    /// * If the project directory doesn't exist, `mongo` will be cloned from
    /// github automatically.  You can adjust where is is cloned from by using
    /// the `--url` flag at the command line.
    /// * If the project does exist, the requested branch is updated via
    /// `update_branch` to prepare for the rest of the build cycle.
    fn scm(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let base = Path::new(cfg.get_dir());
        let u = if self.url.is_empty() {
            "git@github.com:mongodb/mongo.git"
        } else {
            self.url.as_slice()
        };

        let mut res: Result<u8,u8> = if !base.join(cfg.get_project()).exists() {
            GitCommand::new()
                .wd(base.clone())
                .verbose(true)
                .clone(Some(vec!["--recursive", u]), to_res())
        } else {
            Ok(0)
        };

        res = if res.is_ok() {
            GitCommand::new()
                .wd(base.join(cfg.get_project()))
                .verbose(true)
                .update_branch(self.config.get_branch())
        } else {
            res
        };

        res
    }

    /// Not yet implemented
    fn clean(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let mut jobs = String::from_str("-j");
        jobs.push_str(usable_cores().to_string().as_slice());
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        let mut cmd = CommandExt::new("scons");
        cmd.wd(&wd);
        cmd.arg(jobs.as_slice());
        cmd.arg("-c");
        cmd.exec(to_res())
    }

    /// Remove the dirty file.
    fn configure(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        fs::unlink(&Path::new(wd.join("dirty"))).unwrap_or_else(|why| {
            println!("{}", why);
        });
        Ok(0)
    }

    /// Not yet implemented
    fn make(&self) -> Result<u8,u8> {
        let cfg = &self.config;

        // Setup jobs
        let mut jobs = String::from_str("-j");
        jobs.push_str(usable_cores().to_string().as_slice());

        // Working directory
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());

        // scons
        let mut cmd = CommandExt::new("scons");
        cmd.wd(&wd);
        cmd.header(true);

        let mut b = String::from_str("mongo-");
        b.push_str(cfg.get_branch());

        let ip = Path::new("/opt").join(b);
        let mut prefix = String::from_str("--prefix=");
        prefix.push_str(ip.as_str().unwrap());

        cmd.arg(prefix.as_slice());
        cmd.arg(jobs.as_slice());
        cmd.arg("--release");
        cmd.arg("--ssl");
        cmd.arg("--variant-dir=build");
        cmd.arg("--c++11");
        cmd.arg("--use-system-tcmalloc");
        cmd.arg("--use-system-pcre");
        cmd.arg("--use-system-snappy");
        cmd.arg("--use-system-stemmer");

        if is_v26(cfg.get_branch()) {
            let incp = Path::new(env!("HOME")).join("lib/v8/3.12.19/include");
            let libp = Path::new(env!("HOME")).join("lib/v8/3.12.19/lib");
            let mut incarg = String::from_str("--cpppath=");
            incarg.push_str(incp.as_str().unwrap());
            let mut libarg = String::from_str("--libpath=");
            libarg.push_str(libp.as_str().unwrap());

            cmd.arg("--use-system-v8");
            cmd.arg(incarg.as_slice());
            cmd.arg(libarg.as_slice());
        }

        // Targets
        if self.targets.is_empty() {
            cmd.arg("all");
        } else {
            if self.targets.contains(&"all".to_string()) {
                cmd.arg("all");
            } else {
                for target in self.targets.iter() {
                    match target.as_slice() {
                        "core" | "tools" => {
                            cmd.arg(target.as_slice());
                        },
                        _                => {},
                    }
                }
            }
        }

        if cfg.get_test() {
            cmd.arg("test");

            if self.smoke {
                cmd.arg("smoke");
            }

            if self.smokec {
                cmd.arg("smokeCppUnitTests");
            }

            if self.smokej {
                cmd.arg("smokeJsCore");
            }
        }

        if self.lint {
            cmd.arg("lint");
        }

        cmd.exec(to_res())
    }

    /// Not yet implemented
    fn test(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Not yet implemented
    fn install(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let mut sgarg = String::from_str("scons");
        sgarg.push_str(" -j");
        sgarg.push_str(usable_cores().to_string().as_slice());
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        let mut cmd = CommandExt::new("sudo");
        cmd.wd(&wd);
        cmd.header(true);
        cmd.arg("sg");
        cmd.arg("mongo");
        cmd.arg("-c");

        let mut b = String::from_str("mongo-");
        b.push_str(cfg.get_branch());

        let ip = Path::new("/opt").join(b);
        sgarg.push_str(" --prefix=");
        sgarg.push_str(ip.as_str().unwrap());
        sgarg.push_str(" --release");
        sgarg.push_str(" --ssl");
        sgarg.push_str(" --variant-dir=build");
        sgarg.push_str(" --c++11");
        sgarg.push_str(" --use-system-tcmalloc");
        sgarg.push_str(" --use-system-pcre");
        sgarg.push_str(" --use-system-snappy");
        sgarg.push_str(" --use-system-stemmer");

        if is_v26(cfg.get_branch()) {
            let incp = Path::new(env!("HOME")).join("lib/v8/3.12.19/include");
            let libp = Path::new(env!("HOME")).join("lib/v8/3.12.19/lib");
            let mut incarg = String::from_str(" --cpppath=");
            incarg.push_str(incp.as_str().unwrap());
            let mut libarg = String::from_str(" --libpath=");
            libarg.push_str(libp.as_str().unwrap());

            sgarg.push_str(" --use-system-v8");
            sgarg.push_str(incarg.as_slice());
            sgarg.push_str(libarg.as_slice());
        }

        // Targets
        if self.targets.is_empty() {
            sgarg.push_str(" all");
        } else {
            if self.targets.contains(&"all".to_string()) {
                sgarg.push_str(" all");
            } else {
                for target in self.targets.iter() {
                    match target.as_slice() {
                        "core" | "tools" => {
                            sgarg.push_str(" ");
                            sgarg.push_str(target.as_slice());
                        },
                        _                => {},
                    }
                }
            }
        }

        if cfg.get_test() {
            sgarg.push_str(" test");

            if self.smoke {
                sgarg.push_str(" smoke");
            }

            if self.smokec {
                sgarg.push_str(" smokeCppUnitTests");
            }

            if self.smokej {
                sgarg.push_str(" smokeJsCore");
            }
        }

        if self.lint {
            sgarg.push_str(" lint");
        }

        sgarg.push_str(" install");
        cmd.arg(sgarg.as_slice());

        cmd.exec(to_res())
    }

    /// Not yet implemented
    fn cleanup(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Show the docopt USAGE string on stdout.
    fn help(&self) -> Result<u8,u8> {
        println!("{}", USAGE);
        Ok(0)
    }

    /// Show the crate version on stdout.
    fn version(&self) -> Result<u8,u8> {
        println!("{} {} mongo-rub {}", now(), sha(), branch());
        Ok(0)
    }
}

#[cfg(test)]
mod test {
    use buildable::{Buildable,BuildConfig};
    use super::{MongoRub,is_v26};

    fn check_mr(mr: &MongoRub) {
        assert_eq!(mr.prefix, "");
        assert_eq!(mr.url, "");
    }

    fn check_bc(bc: &BuildConfig, lc: &Vec<&str>) {
        let mut tdir = env!("HOME").to_string();
        tdir.push_str("/projects");
        assert_eq!(bc.get_lifecycle(), lc);
        assert_eq!(bc.get_dir().as_str().unwrap(), tdir.as_slice());
        assert_eq!(bc.get_project(), "mongo");
        assert_eq!(bc.get_branch(), "master");
        assert!(!bc.get_test());
    }

    #[test]
    fn test_new() {
        let mr = MongoRub::new();
        check_mr(&mr);
    }

    #[test]
    fn test_version() {
        let args = vec!["rub".to_string(),
                        "mongo".to_string(),
                        "--version".to_string()];
        let mut mr = MongoRub::new();
        check_mr(&mr);
        let b = Buildable::new(&mut mr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["version"]);
        assert_eq!(b.version(), Ok(0))
    }

    #[test]
    fn test_help() {
        let args = vec!["rub".to_string(),
                        "mongo".to_string(),
                        "-h".to_string()];
        let mut mr = MongoRub::new();
        check_mr(&mr);
        let b = Buildable::new(&mut mr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["help"]);
        assert_eq!(b.help(), Ok(0))
    }

    #[test]
    fn test_base() {
        let args = vec!["rub".to_string(),
                        "mongo".to_string()];
        let mut mr = MongoRub::new();
        check_mr(&mr);
        let b = Buildable::new(&mut mr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["most"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_scm() {
        let args = vec!["rub".to_string(),
                        "mongo".to_string(),
                        "scm".to_string()];
        let mut mr = MongoRub::new();
        check_mr(&mr);
        let b = Buildable::new(&mut mr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["scm"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_all() {
        let args = vec!["rub".to_string(),
                        "mongo".to_string(),
                        "all".to_string()];
        let mut mr = MongoRub::new();
        check_mr(&mr);
        let b = Buildable::new(&mut mr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["all"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_is_v26() {
        assert!(is_v26("v2.6"));
        assert!(is_v26("v2.6.1"));
        assert!(is_v26("r2.6"));
        assert!(is_v26("r2.6-rc1"));
        assert!(!is_v26("v3.0"));
    }
}
